package ee.ionia.btckit.service

import ee.ionia.btckit.model.Pair
import java.io.IOException
import java.math.BigDecimal

interface PairService {

	@Throws(PairServiceException::class)
	fun createPair(startWith: String): Pair

	fun savePair(pair: Pair): Pair

	@Throws(PairServiceException::class)
	fun getPairWithBalance(publicAddress: String): Pair

	@Throws(IOException::class)
	fun getBalance(publicAddress: String): BigDecimal

}