package ee.ionia.btckit.service

import ee.ionia.btckit.model.Pair
import ee.ionia.btckit.repository.PairRepository
import org.bitcoinj.core.Address
import org.bitcoinj.core.ECKey
import org.bitcoinj.params.MainNetParams
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.IOException
import java.math.BigDecimal
import java.net.URL

@Service("pairService")
class PairServiceImpl : PairService {

	private var pairRepository: PairRepository? = null

	@Value("\${ee.ionia.btckit.service.url.balance}")
	private val balanceUrl: String? = null

	@Value("\${ee.ionia.btckit.service.url.key}")
	private val apiKey: String? = null

	@Throws(PairServiceException::class)
	override fun createPair(startWith: String): Pair {
		val startWith1 = getActualStartWith(startWith)

		var key: ECKey
		var address: Address
		var addressBase58: String

		do {
			key = ECKey()
			address = Address(MainNetParams.get(), key.pubKeyHash)
			addressBase58 = address.toBase58()
		} while (!addressBase58.startsWith(startWith1))

		val pair = Pair()
		pair.publicAddress = addressBase58
		pair.privateKey = key.getPrivateKeyAsWiF(MainNetParams.get())
		pair.balance = getBalance(pair.publicAddress!!)

		return savePair(pair)
	}

	private fun getActualStartWith(startWith: String): String {
		val startWith1 = "1" + startWith.take(3)
		return if (startWith1.matches(Regex("[a-zA-Z0-9]+"))) startWith1 else ""
	}

	override fun savePair(pair: Pair): Pair {
		return pairRepository!!.save(pair)
	}

	@Throws(PairServiceException::class)
	override fun getPairWithBalance(publicAddress: String): Pair {
		val pairOpt = pairRepository!!.findById(publicAddress)
		var pair: Pair? = null
		if (pairOpt.isPresent()) {
			pair = pairOpt.get()
		} else {
			pair = Pair()
			pair.publicAddress = publicAddress
		}
		pair.balance = getBalance(publicAddress)
		return savePair(pair)
	}

	override fun getBalance(publicAddress: String): BigDecimal {
		var balance: BigDecimal = BigDecimal.ZERO
		val url = getBalanceCheckUrl(publicAddress)
		val balanceString: String?
		try {
			balanceString = URL(url).readText()
			balance = BigDecimal(balanceString!!).divide(BigDecimal("100000000"))
		} catch (ex: IOException) {
			log.error(publicAddress + " failed" + ": " + ex.message)
		}
		return balance
	}

	@Autowired
	@Qualifier("pairRepository")
	fun setPairRepository(pairRepository: PairRepository) {
		this.pairRepository = pairRepository
	}

	private fun getBalanceCheckUrl(publicAddress: String): String {
		var url = balanceUrl + publicAddress
		if (apiKey != null) {
			url = "$url?key=$apiKey"
		}
		return url
	}

	companion object {

		private val log = LoggerFactory.getLogger(PairServiceImpl::class.java)
	}

}