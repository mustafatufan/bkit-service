package ee.ionia.btckit.repository

import ee.ionia.btckit.model.Pair
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository("pairRepository")
interface PairRepository : JpaRepository<Pair, String>