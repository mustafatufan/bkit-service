package ee.ionia.btckit.controller

import ee.ionia.btckit.model.Pair
import ee.ionia.btckit.service.PairService
import ee.ionia.btckit.service.PairServiceException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import java.util.ArrayList

@Controller
@RequestMapping("/")
class PairController {

	private var pairService: PairService? = null

	@RequestMapping(value = "/createPair", method = [RequestMethod.GET], produces = ["application/json"])
	@ResponseBody
	fun createPair(@RequestParam(value = "startWith", defaultValue = "", required = false) startWith: String): Pair? {
		try {
			return pairService!!.createPair(startWith)
		} catch (ex: PairServiceException) {
			log.error(ex.message, ex)
		}

		return null
	}

	@RequestMapping(value = "/createPairList", method = [RequestMethod.GET], produces = ["application/json"])
	@ResponseBody
	fun createPairList(@RequestParam(value = "startWith", required = false) startWith: String, @RequestParam("size") size: Int): List<Pair> {
		val list = ArrayList<Pair>()
		try {
			for (i in 0 until size) {
				list.add(pairService!!.createPair(startWith))
			}
		} catch (ex: PairServiceException) {
			log.error(ex.message, ex)
		}

		return list
	}

	@RequestMapping(value = "/getBalance", method = [RequestMethod.GET], produces = ["application/json"])
	@ResponseBody
	fun getBalance(@RequestParam("publicAddress") publicAddress: String): Pair? {
		try {
			return pairService!!.getPairWithBalance(publicAddress)
		} catch (ex: PairServiceException) {
			log.error(ex.message, ex)
		}

		return null
	}

	@Autowired
	@Qualifier("pairService")
	fun setPairService(pairService: PairService) {
		this.pairService = pairService
	}

	companion object {

		private val log = LoggerFactory.getLogger(PairController::class.java)
	}

}