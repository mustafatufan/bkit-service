package ee.ionia.btckit

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BtckitApplication

fun main(args: Array<String>) {
	runApplication<BtckitApplication>(*args)
}
