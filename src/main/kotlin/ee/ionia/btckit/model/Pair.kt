package ee.ionia.btckit.model

import org.hibernate.annotations.CreationTimestamp
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "pair")
class Pair {

	@Id
	@Column(name = "public_address")
	var publicAddress: String? = null
		get() = field
		set(value) {
			field = value
		}


	@Column(name = "private_key", nullable = true)
	var privateKey: String? = null
		get() = field
		set(value) {
			field = value
		}

	@Column(name = "balance", nullable = true, precision = 20, scale = 8)
	var balance: BigDecimal? = null
		get() = field
		set(value) {
			field = value
		}

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_at")
	val createAt: Date? = null
		get() = field


}