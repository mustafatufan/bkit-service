<h1><img width="32" height="32" src="https://btckit.io/images/logo.png"> Btckit</h1>

**Btckit** is a hobby project that aims to serve free Bitcoin tools.
## Btckit Service
[**btckit.io**](https://btckit.io) is a website that serves [**Btckit-Service**](https://github.com/btckit/btckit-service) and hosted on Github Pages.
## Usage
### `/createPair`
You can create a random Bitcoin public address and private key pair.

---
**Request** `http://api.btckit.io/createPair`

**Response**
```json
{
	"publicAddress":"1Dv2R4Kawu5qp7BbZp5RMJaNtY7NmEA7RD",
	"privateKey":"L4hnVE5pXkztm23KVeZX8emU4FHYPtYsw1t3SNj9q1Pk1xB9tFD6",
	"balance":0
}
```

### `/createPair?startWith=[STARTING_STRING]`
You can create a random Bitcoin public address and private key pair by deciding public address' first three characters.

---
**Request** `http://btckit.io/createPair?startWith=ABC`

**Response**
```json
{
	"publicAddress":"1ABCtyqC3mwCEQHB6oycrD2m1zpn2gGUXq",
	"privateKey":"L328x1XJWzSJpX9Xn69Xit5vFwRiiscGRcoZtr2HXZzW4NBDFCcM",
	"balance":0
}
```

### `/getBalance?publicAddress=[PUBLIC_ADDRESS]`
You can get balance of any Bitcoin address with this request. If database include private key of this public address then you could have it also.

---
**Request** `http://api.btckit.io/getBalance?publicAddress=18PC53PMZoLdzPZWx3TSJeXWe8K3oSxsKu`

**Response**
```json
{
	"publicAddress":"18PC53PMZoLdzPZWx3TSJeXWe8K3oSxsKu",
	"privateKey":"KyTtjGsLMq88XTu7rv9ihosSyK3yxh3KM2mgRkecz9VRJ63sPBm4",
	"balance":0
}
```
---
## Contributing
Pull requests are welcome.
## License
[MIT](https://choosealicense.com/licenses/mit/)